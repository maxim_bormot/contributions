/* eslint-disable react/forbid-prop-types */
import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

// eslint-disable-next-line no-unused-vars
const style = theme => ({
  container: {
    margin: '0 auto',
    paddingLeft: 15,
    paddingRight: 15,
  },
})

const Container = ({ children, classes }) =>
  <div className={classes.container} style={{ maxWidth: 1000 }}>
    <div>
      {children}
    </div>
  </div>

Container.propTypes = {
  children: PropTypes.node.isRequired,
  classes: PropTypes.object.isRequired,
}

export default withStyles(style)(Container)
