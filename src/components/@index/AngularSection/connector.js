import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as angular from '../../../actions/angular.action'

const mapStateToProps = state => ({
  angular: state.angularReducer.angular,
})

const mapDispatchToProps = dispatch => ({
  actions: {
    angular: bindActionCreators(angular, dispatch),
  },
})

export default connect(mapStateToProps, mapDispatchToProps)
