/* eslint-disable padded-blocks */
import React from 'react'
import Spinner from 'react-spinner-material'
import PropTypes from 'prop-types'
import Paper from '@material-ui/core/es/Paper/Paper'
import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/es/Grid/Grid'
import Typography from '@material-ui/core/es/Typography/Typography'
import connector from './connector'
import AngularChip from '../AngularChip'

const style = theme => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing.size3,
    padding: theme.spacing.size3,
  },

  logo: {
    width: '100%',
    paddingRight: theme.spacing.size2,
  },

  chips: {
    marginTop: theme.spacing.size2,
  },
})

class AngularSection extends React.Component {

  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    this.props.actions.angular.load()
  }

  render() {
    const { angular, classes } = this.props

    if (!angular) {
      return (
        <Spinner
          size={120}
          spinnerColor="#333"
          spinnerWidth={2}
          visible
        />
      )
    }

    return (
      <Paper className={classes.root}>
        <Grid container>
          <Grid item container justify="center" alignItems="center">
            <Grid item md={3}><img src={angular.avatar_url} alt="logo" className={classes.logo} /></Grid>
            <Grid item md={9}>
              <Typography gutterBottom variant="headline">{angular.name}</Typography>
              <Typography>Blog: <a href={angular.blog}>{angular.blog}</a></Typography>
              <Typography>GitHub: <a href={angular.html_url}>{angular.html_url}</a></Typography>
            </Grid>
          </Grid>
          <Grid item container justify="space-between" className={classes.chips}>
            <AngularChip label="public repositories">{angular.public_repos}</AngularChip>
            <AngularChip label="followers">{angular.followers}</AngularChip>
            <AngularChip label="following">{angular.following}</AngularChip>
            <AngularChip label="type">{angular.type}</AngularChip>
          </Grid>
        </Grid>
      </Paper>
    )
  }

}

AngularSection.propTypes = {
  classes: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  angular: PropTypes.object.isRequired,
}

export default connector(withStyles(style)(AngularSection))
