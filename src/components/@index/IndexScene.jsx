import React from 'react'
// import PropTypes from 'prop-types'
import AngularSection from './AngularSection'
import ContributorsSection from './ContributorsSection'
import Container from '../Container'

const IndexScene = () =>
  <section>
    <Container>
      <AngularSection />
      <ContributorsSection />
    </Container>
  </section>

IndexScene.propTypes = {}

export default IndexScene
