import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Chip from '@material-ui/core/es/Chip/Chip'

// eslint-disable-next-line no-unused-vars
const styles = theme => ({
  label: {
    fontWeight: '500',
  },
})

const AngularChip = ({
  classes, label, children, ...props
}) =>
  <Chip component="div" {...props} label={<span><span className={classes.label}>{label}:</span> {children}</span>} />

AngularChip.propTypes = {
  label: PropTypes.node,
  children: PropTypes.node,
  classes: PropTypes.object.isRequired,
}

AngularChip.defaultProps = {
  label: '',
  children: '',
}

export default withStyles(styles)(AngularChip)
