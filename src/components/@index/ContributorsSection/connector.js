import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as contributors from '../../../actions/contributors.action'

const mapStateToProps = state => ({
  sort: state.contributorsReducer.sort,
  contributors: state.contributorsReducer.contributors,
})

const mapDispatchToProps = dispatch => ({
  actions: {
    contributors: bindActionCreators(contributors, dispatch),
  },
})

export default connect(mapStateToProps, mapDispatchToProps)
