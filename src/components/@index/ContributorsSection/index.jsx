/* eslint-disable function-paren-newline */
import React from 'react'
import PropTypes from 'prop-types'
import Spinner from 'react-spinner-material'
import List from '@material-ui/core/es/List/List'
import { withStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/es/Paper/Paper'
import Contributor from './Contributor'
import connector from './connector'
import SortSection from './SortSection'

const style = theme => ({
  root: {
    marginTop: theme.spacing.size3,
    padding: theme.spacing.size3,
  },

  tabs: {
    marginTop: theme.spacing.size3,
  },
})

class ContributorsSection extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    this.props.actions.contributors.load()
  }

  render() {
    const { classes, contributors } = this.props

    if (!contributors.length) {
      return (
        <Spinner
          size={120}
          spinnerColor="#333"
          spinnerWidth={2}
          visible
        />
      )
    }

    return (
      <Paper className={classes.root}>
        <SortSection />
        <List>
          {contributors.map((contributor, index) =>
            <Contributor contributor={contributor} key={index} />,
          )}
        </List>
      </Paper>
    )
  }
}

ContributorsSection.propTypes = {
  classes: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  contributors: PropTypes.array.isRequired,
}

export default connector(withStyles(style)(ContributorsSection))
