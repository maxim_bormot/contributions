import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Select from '@material-ui/core/es/Select/Select'
import MenuItem from '@material-ui/core/es/MenuItem/MenuItem'
import Typography from '@material-ui/core/es/Typography/Typography'
import FormControl from '@material-ui/core/es/FormControl/FormControl'
import connector from './connector'

/* eslint-disable no-unused-vars */
const styles = theme => ({
  root: {
    display: 'flex',
    alignItems: 'baseline',
  },

  select: {
    width: 150,
  },
})

const SortSection = ({ classes, actions, sort }) =>
  <section className={classes.root}>

    <Typography variant="subheading">Sort by: &nbsp;</Typography>


    <FormControl>

      <Select
        value={sort}
        className={classes.select}
        onChange={({ target }) => actions.contributors.sortBy(target.value)}
        inputProps={{ name: 'contributors-sort', id: 'contributors-sort' }}
      >

        <MenuItem value="contributions">Contributions</MenuItem>

        <MenuItem value="followers">Followers</MenuItem>

        <MenuItem value="gists">Gists</MenuItem>

      </Select>

    </FormControl>


  </section>

SortSection.propTypes = {
  sort: PropTypes.string.isRequired,
  actions: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
}

export default connector(withStyles(styles)(SortSection))
