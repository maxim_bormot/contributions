/* eslint-disable react/jsx-curly-spacing */
import React from 'react'
import PropTypes from 'prop-types'
import ListItem from '@material-ui/core/es/ListItem/ListItem'
import Typography from '@material-ui/core/es/Typography/Typography'
import ListItemText from '@material-ui/core/es/ListItemText/ListItemText'
import Avatar from '@material-ui/core/es/Avatar/Avatar'
import { withStyles } from '@material-ui/core/styles/index'
import AngularChip from '../AngularChip'

const styles = theme => ({
  chip: {
    margin: theme.spacing.size1,
  },
  rank: {
    paddingRight: theme.spacing.size3,
  },
})

const Contributor = ({ classes, contributor }) =>
  <ListItem>
    <Typography className={classes.rank} variant="headline">#{contributor.rank}</Typography>
    <Avatar sizes="300px" alt={contributor.login} src={contributor.avatar_url} />
    <ListItemText
      primary={contributor.login}
      secondary={
        <div>
          <AngularChip
            className={classes.chip}
            label="contributions"
          >
            {contributor.contributions}
          </AngularChip>
          <AngularChip
            className={classes.chip}
            label="followers"
          >
            {contributor.followers}
          </AngularChip>
        </div>
      }
    />
  </ListItem>

Contributor.propTypes = {
  classes: PropTypes.object.isRequired,
  contributor: PropTypes.object.isRequired,
}

export default withStyles(styles)(Contributor)
