import { combineReducers } from 'redux'
import angularReducer from './angular.reducer'
import contributorsReducer from './contributors.reducer'

// noinspection JSUnusedGlobalSymbols
export default combineReducers({
  angularReducer,
  contributorsReducer,
})
