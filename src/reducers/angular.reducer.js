import {
  GET_ORGANISATION_FULFILLED,
  GET_ORGANISATION_PENDING,
  GET_ORGANISATION_REJECTED,
} from '../actions/angular.action'

const initialState = {
  loading: false,
  error: null,
  angular: {},
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_ORGANISATION_PENDING:
      return {
        ...state,
        loading: true,
      }

    case GET_ORGANISATION_FULFILLED:
      return {
        ...state,
        angular: payload,
        loading: false,
      }

    case GET_ORGANISATION_REJECTED:
      return {
        ...state,
        loading: false,
        error: payload,
      }
    default: {
      return state
    }
  }
}
