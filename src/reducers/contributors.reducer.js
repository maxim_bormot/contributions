import {
  GET_CONTRIBUTORS_PENDING,
  GET_CONTRIBUTORS_FULFILLED,
  GET_CONTRIBUTORS_REJECTED, SORT_CONTRIBUTORS,
} from '../actions/contributors.action'
import Contributors from '../logic/Contributors'

const initialState = {
  loading: false,
  error: null,
  sort: 'contributions',
  contributors: [],
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_CONTRIBUTORS_PENDING:
      return {
        ...state,
        loading: true,
      }

    case GET_CONTRIBUTORS_FULFILLED: {
      const contributors = new Contributors(payload)
      return {
        ...state,
        contributors: contributors
          .sortBy(state.sort)
          .groupBy(state.sort)
          .rankBy(state.sort)
          .get(),
        loading: false,
      }
    }

    case GET_CONTRIBUTORS_REJECTED:
      return {
        ...state,
        loading: false,
        error: payload,
      }

    case SORT_CONTRIBUTORS: {
      const contributors = new Contributors(state.contributors)
      return {
        ...state,
        sort: payload,
        contributors: contributors
          .sortBy(payload)
          .groupBy(payload)
          .rankBy(payload)
          .get(),
      }
    }

    default: {
      return state
    }
  }
}
