/* eslint-disable prefer-destructuring */
class Contributors {
  constructor(contributors) {
    this.contributors = contributors

    this.sortBy = this.sortBy.bind(this)
    this.rankBy = this.rankBy.bind(this)
    this.groupBy = this.groupBy.bind(this)
    this.get = this.get.bind(this)
  }

  sortBy(name) {
    this.contributors.sort((a, b) => {
      if (a[name] < b[name]) return 1
      if (a[name] > b[name]) return -1
      return 0
    })

    return this
  }

  rankBy(name) {
    let rank = 0
    let contributions = 1

    this.contributors = this.contributors.map(contributor => {
      if (contributions !== contributor[name]) {
        rank++
        contributions = contributor[name]
      }
      contributor.rank = rank

      return contributor
    })

    return this
  }

  groupBy(name) {
    const result = []
    this.contributors.reduce((res, value) => {
      if (!res[value.id]) {
        res[value.id] = {
          ...value,
          [name]: 0,
          id: value.id,
        }
        result.push(res[value.id])
      }
      res[value.id][name] += value[name]
      return res
    }, {})

    this.contributors = result

    return this
  }

  get() {
    return this.contributors
  }
}

export default Contributors
