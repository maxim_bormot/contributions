import Angular from '../services/github/Angular'

export const GET_CONTRIBUTORS = 'GET_CONTRIBUTORS'
export const GET_CONTRIBUTORS_PENDING = 'GET_CONTRIBUTORS_PENDING'
export const GET_CONTRIBUTORS_FULFILLED = 'GET_CONTRIBUTORS_FULFILLED'
export const GET_CONTRIBUTORS_REJECTED = 'GET_CONTRIBUTORS_REJECTED'
export const SORT_CONTRIBUTORS = 'SORT_CONTRIBUTORS'

// noinspection JSUnusedGlobalSymbols
export const load = () => ({
  type: GET_CONTRIBUTORS,
  payload: Angular.contributors(),
})

// noinspection JSUnusedGlobalSymbols
export const sortBy = value => ({
  type: SORT_CONTRIBUTORS,
  payload: value,
})
