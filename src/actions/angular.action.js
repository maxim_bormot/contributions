import Angular from '../services/github/Angular'

export const GET_ORGANISATION = 'GET_ORGANISATION'
export const GET_ORGANISATION_PENDING = 'GET_ORGANISATION_PENDING'
export const GET_ORGANISATION_FULFILLED = 'GET_ORGANISATION_FULFILLED'
export const GET_ORGANISATION_REJECTED = 'GET_ORGANISATION_REJECTED'


// noinspection JSUnusedGlobalSymbols
export const load = () => ({
  type: GET_ORGANISATION,
  payload: Angular.basicInfo(),
})

