/* eslint-disable class-methods-use-this */
import flattenDeep from 'lodash/flattenDeep'
import to from '../../utils/to'
import http from './Http'

class Angular {
  basicInfo() {
    return http.get('/orgs/angular')
  }

  repos() {
    return http.get('/orgs/angular/repos')
  }

  async contributors() {
    const angularRepos = await this.repos()

    const contributors = angularRepos.map(async (repo) => {
      const contributor = await http.get(repo.contributors_url)

      const followers = await http.get(contributor[0].followers_url)

      contributor[0].followers = followers.length

      return contributor[0]
    })
    const [err, result] = await to(Promise.all(contributors))
    if (err) throw err

    return flattenDeep(result)
  }
}

export default new Angular()
