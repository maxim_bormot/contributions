import axios from 'axios'
import to from '../../utils/to'
import Cache from '../Cache'

class Http {
  constructor() {
    this.instance = axios.create({
      baseURL: 'https://api.github.com',
      timeout: 1000,
      adapter: (axios.defaults.adapter),
    })
  }

  async get(url) {
    if (Cache.has(url)) return Cache.get(url)

    const [err, response] = await to(this.instance.get(url))

    if (err) throw err

    Cache.put(url, response.data)

    return response.data
  }
}

export default new Http()

